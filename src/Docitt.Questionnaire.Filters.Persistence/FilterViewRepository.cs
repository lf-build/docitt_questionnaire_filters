﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using MongoDB.Bson;

namespace Docitt.Questionnaire.Filters.Persistence
{
    public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository
    {
        static FilterViewRepository()
        {
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IApplicant, Applicant>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ISection, Section>());

            BsonClassMap.RegisterClassMap<FilterView>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Assignees)
               .SetSerializer(new DictionaryInterfaceImplementerSerializer<Dictionary<string, string>>
                   (
                       DictionaryRepresentation.ArrayOfDocuments
                   )
               );
                map.MapMember(m => m.LastReminder).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.LastLoggedIn).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

                var type = typeof(FilterView);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Applicant>(map =>
            {
                map.AutoMap();
            });

            BsonClassMap.RegisterClassMap<Section>(map =>
            {
                map.AutoMap();
            });
            
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                var type = typeof(TimeBucket);
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer());
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public FilterViewRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "questionnaire-filters")
        {
            CreateIndexIfNotExists("applicationNumber", Builders<IFilterView>.IndexKeys.Ascending(t => t.TenantId).Ascending(i => i.TemporaryApplicationNumber), true);
            CreateIndexIfNotExists("applicationNumber_Assignees",
                Builders<IFilterView>.IndexKeys.Ascending(t => t.TenantId).Ascending(i => i.TemporaryApplicationNumber)
                    .Ascending(i => i.Assignees));
        }

        public void AddOrUpdate(IFilterView view)
        {
            if (view == null)
                throw new ArgumentNullException(nameof(view));

            System.Linq.Expressions.Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.TemporaryApplicationNumber == view.TemporaryApplicationNumber;

            view.TenantId = TenantService.Current.Id;

            var result = GetApplicationByApplicationNumber(view.TemporaryApplicationNumber).Result;
            if(result != null)
            {
                // Ensure that the status can be updated only in incremental order 
                if(result.Status > view.Status)
                    view.Status = result.Status;
            }

            try
            {
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> {IsUpsert = true});
            }
            catch (MongoCommandException)
            {
                //Retry one more time, if fail it will throw excepion
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
            catch (MongoWriteException)
            {
                //Retry one more time, if fail it will throw excepion
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
        }

        public async Task<IFilterView> GetByUserName(string entityId, string username)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { username });
            var filterByEntityId = builders.Where(db => db.TemporaryApplicationNumber == entityId);
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByEntityId))
              .FirstOrDefaultAsync();
        }
        public async Task<IEnumerable<IFilterView>> GetAllApplications()
        {
            return await Query.ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetApplications(FormStatus status)
        {
            return await Query.Where(x => x.Status == status).ToListAsync();
        }

        public async Task<IFilterView> GetApplicationByApplicationNumber(string  applicationNumber)
        {
            return await Query.FirstOrDefaultAsync(x => x.TemporaryApplicationNumber == applicationNumber);
        }


        // TAN = Temporary Application Number
        public async Task<IFilterView> GetApplicationByEntityId(string entityId)
        {
            return await Query.FirstOrDefaultAsync(x => x.TemporaryApplicationNumber == entityId);
        }

/*
        public async Task<IFilterView> GetApplicationByUsername(string userName)
        {
            return await Query.FirstOrDefaultAsync(p => p.Applicants.Any(a => a.ApplicantType == ApplicantType.Borrower.ToString() && a.UserName == userName));
        }
*/

        public async Task<IEnumerable<IFilterView>> GetUserWiseApplications(string userName)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });         
            var result = await Collection.Find(builders.And(filterByTenantId, filterByUserName))
                .SortByDescending(a => a.TemporaryApplicationNumber)
                .ToListAsync();
            
            if(result == null || result.Count == 0)
            {
                result = await Query.Where(x => x.TenantId == TenantService.Current.Id
                            && x.Applicants.Any((y => y.UserName == userName))).ToListAsync();
            }
            return result;
        }

        public async Task<IEnumerable<IFilterView>> GetUserAndStatusWiseApplications(string userName, FormStatus status)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });
            var filterByStatus = builders.Where(db => db.Status == status);
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus))
                .SortByDescending(a => a.TemporaryApplicationNumber)
                .ToListAsync();
        }

        public async Task<bool> MapInviteIdToUsername(string inviteId, string userName, string temporaryApplicationNumber)
        {
            IFilterView result;
            var view = await Query.FirstOrDefaultAsync(x => x.TenantId == TenantService.Current.Id &&
                            x.TemporaryApplicationNumber == temporaryApplicationNumber
                            && x.Applicants.Any((y => y.UserName == inviteId)));

            if (view == null) 
                return false;
            
            foreach(var applicant in view.Applicants)
            {
                if(applicant.UserName == inviteId)
                {
                    applicant.UserName = userName;
                }
            }

            System.Linq.Expressions.Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.TemporaryApplicationNumber == view.TemporaryApplicationNumber;

            try
            {
                result = Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
            catch (MongoWriteException)
            {
                //Retry one more time, if fail it will throw excepion
                result = Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }

            return result != null;
        }

        public void UpdateLastLoggedInByUserName(string userName, DateTimeOffset? lastLoggedIn)
        {
            System.Linq.Expressions.Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.Applicants.Any(a => a.ApplicantType == ApplicantType.Borrower.ToString() && a.UserName == userName);

            Collection.UpdateMany(query,
                new UpdateDefinitionBuilder<IFilterView>().Set(a => a.LastLoggedIn, lastLoggedIn));
        }
        
        public void UpdateLastReminderSent(string applicationNumber, DateTimeOffset lastReminderSent)
        {
            System.Linq.Expressions.Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.TemporaryApplicationNumber == applicationNumber;

            Collection.UpdateMany(query,
                new UpdateDefinitionBuilder<IFilterView>().Set(a => a.LastReminder, lastReminderSent));
        }
        
        public void UpdateStatus(string applicationNumber, FormStatus status)
        {
            System.Linq.Expressions.Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.TemporaryApplicationNumber == applicationNumber;

            Collection.UpdateMany(query,
                new UpdateDefinitionBuilder<IFilterView>().Set(a => a.Status, status));
        }

         public async Task<string> GetUserNameByApplicationNumberAndRole(string applicationNumber,string role)
        {
           
            IFilterView result;
             result = await Query.FirstOrDefaultAsync(x => x.TenantId == TenantService.Current.Id &&
                            x.TemporaryApplicationNumber == applicationNumber);

           if(result != null)
           {
               var user = result.Assignees.Where(x => x.Value.Equals(role,StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
               if(user.Key != null)
               {
               return user.Key;
               }
           }
           
           return string.Empty;
        }

    }
}