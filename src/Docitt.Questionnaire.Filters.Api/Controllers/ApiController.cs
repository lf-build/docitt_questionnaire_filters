﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire.Filters.Api.Controllers
{
    /// <summary>
    /// Represents ApiController class.
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public ApiController(IQuestionnaireFilterService service, ILogger logger) : base(logger)
        {
            if (service == null) throw new ArgumentException($"{nameof(service)} is madatory");

            Service = service;
        }

        private IQuestionnaireFilterService Service { get; }

        /// <summary>
        /// Get All applications.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/all")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetAllApplications()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAllApplications()));
        }

        /// <summary>
        /// Get applications based on status.
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet("/{status}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(404)]
#endif
        public async Task<IActionResult> GetApplications(string status)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await Service.GetApplications(status)));
            }
            catch (Exception ex)
            {
#if DOTNET2
                return new NotFoundObjectResult(ex.Message);
#else
                return new HttpNotFoundObjectResult(ex.Message);
#endif
            }
        }

        /// <summary>
        /// Gets user wise questionnaire application.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/mine")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetUserWiseApplications()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUserWiseApplications()));
        }

        /// <summary>
        /// Gets user and status wise questionnaire application.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/mine/{status}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetUserAndStatusWiseApplications(string status)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUserAndStatusWiseApplications(status)));
        }

        /// <summary>
        /// Gets Non-submitted application Information.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/{applicationId}/temporary")]
#if DOTNET2
        [ProducesResponseType(typeof(IFilterView), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetNonSubmittedApplication(string applicationId)
        {
            try
            {
                Logger.Info("Starting the GetNonSubmittedApplication Method...");
                var result = await Service.GetNonSubmittedApplication(applicationId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetNonSubmittedApplication Method raised an error: {ex}");
                return ErrorResult.BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get application data with leads..
        /// </summary>    
        /// <returns></returns>
        [HttpGet("application-with-lead/all")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(404)]
#endif
        public async Task<IActionResult> GetAllApplicationsWithLeads()
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await Service.GetAllApplicationsWithLeads()));
            }
            catch (Exception ex)
            {
#if DOTNET2
                return new NotFoundObjectResult(ex.Message);
#else
                return new HttpNotFoundObjectResult(ex.Message);
#endif
            }
        }

        /// <summary>
        /// Get application data with leads..
        /// </summary>    
        /// <returns></returns>
        [HttpGet("application-with-lead/{status}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(404)]
#endif
        public async Task<IActionResult> GetAllApplicationsWithOpenLeads(string status)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await Service.GetAllApplicationsLeadsWithStatus(status)));
            }
            catch (Exception ex)
            {
#if DOTNET2
                return new NotFoundObjectResult(ex.Message);
#else
                return new HttpNotFoundObjectResult(ex.Message);
#endif
            }
        }

        /// <summary>
        /// Get application data with leads..
        /// </summary>    
        /// <returns></returns>
        [HttpGet("application-with-lead/mine")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(404)]
#endif
        public async Task<IActionResult> GetUserWiseApplicationsWithLeads()
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await Service.GetUserWiseApplicationsWithLeads()));
            }
            catch (Exception ex)
            {
#if DOTNET2
                return new NotFoundObjectResult(ex.Message);
#else
                return new HttpNotFoundObjectResult(ex.Message);
#endif
            }
        }

        /// <summary>
        /// Get questionnaire application data from temporarily application number...
        /// </summary>    
        /// <returns>Application Data</returns>
        [HttpGet("application/{applicationNumber}")]
#if DOTNET2
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(404)]
#endif
        public async Task<IActionResult> GetApplicatioinDetailsWithApplicationNumber(string applicationNumber)
        {
            try
            {
                return await ExecuteAsync(async () => Ok(await Service.GetApplicationByApplicationNumber(applicationNumber)));
            }
            catch (Exception ex)
            {
#if DOTNET2
                return new NotFoundObjectResult(ex.Message);
#else
                return new HttpNotFoundObjectResult(ex.Message);
#endif
            }
        }

        /// <summary>
        /// Assigned me with specific entityType and entityId.
        /// </summary>        
        /// <param name="entityId">The entity id</param>
        /// <returns></returns>
        [HttpGet("{entityId}/assigned/me")]
#if DOTNET2
        [ProducesResponseType(typeof(IAssignedResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> IsAssignedToMe(string entityId)
        {
            return ExecuteAsync(async () => Ok(await Service.IsAssigned(entityId)));
        }

        /// <summary>
        /// Gets user Name wise questionnaire application.
        /// </summary>
        /// <returns></returns>
        [HttpGet("applications/all/mine")]
        [ProducesResponseType(typeof(List<ApplicationWrapperResponse>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserNameWiseAllApplications()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUserNameWiseAllApplications()));
        }

         /// <summary>
        /// Gets user Name wise questionnaire application.
        /// </summary>
        /// <returns></returns>
        [HttpGet("applications/number/mine")]
        [ProducesResponseType(typeof(List<string>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllApplicationNumbersByUser()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAllApplicationNumbersByUser()));
        }

        /// <summary>
        /// Gets user Name wise questionnaire application.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/{temporaryApplicationNumber}/isValidApplication")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> IsValidApplicationForUser(string temporaryApplicationNumber)
        {
            return await ExecuteAsync(async () => Ok(await Service.IsValidApplicationForUser(temporaryApplicationNumber)));
        }

        /// Send Reminder Email
        /// <summary>
        /// <param name="applicationNumber"></param>
        /// <param name="templateName"></param>
        /// <param name="templateVersion"></param>
        /// <param name="payload"></param>
        /// <param name="sendemail"></param>
        /// </summary>
        /// <returns></returns>
        [HttpPost("application/{applicationNumber}/{templateName}/{templateVersion}/{sendemail}")]
        [ProducesResponseType(typeof(ISendReminderEmailResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> SendReminderEmail(string applicationNumber, string templateName ,string templateVersion, bool sendemail, [FromBody]ReminderEmailRequest payload)
        {
            return ExecuteAsync(async () => Ok(await Service.SendReminderEmail(applicationNumber, templateName ,templateVersion, payload, sendemail)));
        }

        /// Send Reminder Email
        /// <summary>
        /// <param name="temporaryApplicationNumber"></param>
        /// <param name="inviteId"></param>
        /// <param name="userName"></param>
        /// </summary>
        /// <returns></returns>
        [HttpPut("/map/inviteid/{inviteId}/{username}/temporaryapplicationnumber/{temporaryApplicationNumber}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult),400)]
        public Task<IActionResult> UpdateUsernameField(string inviteId, string userName ,string temporaryApplicationNumber)
        {
            return ExecuteAsync(async () => Ok(await Service.UpdateUsernameField(inviteId, userName, temporaryApplicationNumber)));
        }


      
        /// <summary>
        /// <param name="temporaryApplicationNumber">application number</param>
        /// <param name="applicantList">List of applicant to be removed</param>  
        /// </summary>
        /// <returns>bool</returns>
        [HttpPut("/remove-applicant/{temporaryApplicationNumber}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult),400)]
        public Task<IActionResult> RemoveApplicantFromApplication([FromBody]string[] applicantList ,string temporaryApplicationNumber)
        {
            return ExecuteAsync(async () => Ok(await Service.RemoveApplicantFromApplication(temporaryApplicationNumber,applicantList)));
        }

       /// <summary>
        /// Gets user wise questionnaire application.
        /// </summary>
        
       /// <param name="userName">supplied userName</param>
       /// <returns>List Of application</returns>
        [HttpGet("/applications-by-user/{userName}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserWiseApplications(string userName)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetApplicationsByUser(userName)));
        }

        /// <summary>
        /// Gets user name by role for application number
        /// </summary>        
       /// <param name="applicationNumber">supplied application number</param>     
        /// <param name="role">supplied role</param>
        /// <returns></returns>
        [HttpGet("/by-role/{role}/{applicationNumber}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserByApplicationNumberAndRole(string applicationNumber,string role)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUserNameByApplicationNumberAndRole(applicationNumber,role)));
        }
    }
}