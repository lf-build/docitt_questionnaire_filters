﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using RestSharp;

namespace Docitt.Questionnaire.Filters.Client
{
    public class QuestionnaireFilterClient : IQuestionnaireFilterService
    {
        public QuestionnaireFilterClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }
        
        public async Task<IEnumerable<IFilterView>> GetAllApplications()
        {
            return await Client.GetAsync<List<FilterView>>($"/all");
        }

        public async Task<IEnumerable<IFilterView>> GetApplications(string status)
        {
             return await Client.GetAsync<List<FilterView>>($"/{status}");
        }

        public async Task<IEnumerable<IFilterView>> GetUserAndStatusWiseApplications(string status)
        {
             return await Client.GetAsync<List<FilterView>>($"/mine/{status}");
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseApplications()
        {
             return await Client.GetAsync<List<FilterView>>($"/mine");
        }

        public async Task<IFilterView> GetNonSubmittedApplication(string applicationId)
        {
             return await Client.GetAsync<FilterView>($"/{applicationId}/temporary");
        }

        public async Task<IEnumerable<IFilterView>> GetAllApplicationsWithLeads()
        {
             return await Client.GetAsync<List<FilterView>>($"/application-with-lead/all");
        }

        public async Task<IAssignedResponse> IsAssigned(string entityId)
        {
            var request = new RestRequest($"/{entityId}/assigned/me", Method.GET);
            return await Client.ExecuteAsync<IAssignedResponse>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAllApplicationsLeadsWithStatus(string status)
        {
             return await Client.GetAsync<List<FilterView>>($"/application-with-lead/{status}");
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseApplicationsWithLeads()
        {
             return await Client.GetAsync<List<FilterView>>($"/application-with-lead/mine");
        }

        public async Task<List<string>> GetAllApplicationNumbersByUser()
        {
             return await Client.GetAsync<List<string>>($"/applications/number/mine");
        }

        public async Task<IFilterView> GetApplicationByApplicationNumber(string applicationNumber)
        {
             return await Client.GetAsync<FilterView>($"/application/{applicationNumber}");
        }

        public async Task<List<IApplicationWrapperResponse>> GetUserNameWiseAllApplications()
        {
             var result = await Client.GetAsync<List<ApplicationWrapperResponse>>($"/applications/all/mine");
             return result.ToList<IApplicationWrapperResponse>();
        }

        public Task<ISendReminderEmailResponse> SendReminderEmail(string applicationNumber, string templateName ,string templateVersion, IReminderEmailRequest payload, bool sendemail=true)
        {
            throw new NotImplementedException();
        }

        public async Task<List<string>> GetUserAllApplications()
        {
             var result = await Client.GetAsync<List<string>>($"/applications/number/mine");
             return result.ToList<string>();
        }
        

        public async Task<bool> IsValidApplicationForUser(string temporaryApplicationNumber)
        {
            var request = new RestRequest($"/{temporaryApplicationNumber}/isValidApplication", Method.GET);
            return await Client.ExecuteAsync<bool>(request);
        }
        
        public async Task<bool> UpdateUsernameField(string inviteId, string userName, string temporaryApplicationNumber)
        {
            var result = await Client.PutAsync<bool>($"/map/inviteid/{inviteId}/{userName}/temporaryapplicationnumber/{temporaryApplicationNumber}", true);
            
            return result.IsSuccessful;
        }

        public async Task<bool> RemoveApplicantFromApplication(string applicationNumber, IEnumerable<string> applicantList)
        {
            var result = await Client.PutAsync($"/remove-applicant/{applicationNumber}", applicantList,true);
            return result.IsSuccessful;
        }

        public async Task<IEnumerable<string>> GetApplicationsByUser(string userName)
        {
            var result = await Client.GetAsync<List<string>>($"/applications-by-user/{userName}");
             return result;
        }

        public async Task<string> GetUserNameByApplicationNumberAndRole(string applicationNumber, string role)
        {
             var result = await Client.GetAsync<dynamic>($"/by-role/{role}/{applicationNumber}");
             return Convert.ToString(result);
        }
    }
}