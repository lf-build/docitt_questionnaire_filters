﻿using LendFoundry.Security.Tokens;

namespace Docitt.Questionnaire.Filters.Client
{
    public interface IQuestionnaireFilterClientFactory
    {
        IQuestionnaireFilterService Create(ITokenReader reader);
    }
}