﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.Questionnaire.Filters.Client
{
    public static class QuestionnaireFilterClientExtensions
    {

        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddQuestionnaireFilters(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IQuestionnaireFilterClientFactory>(p => new QuestionnaireFilterClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IQuestionnaireFilterClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddQuestionnaireFilters(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IQuestionnaireFilterClientFactory>(p => new QuestionnaireFilterClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IQuestionnaireFilterClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddQuestionnaireFilters(this IServiceCollection services)
        {
            services.AddTransient<IQuestionnaireFilterClientFactory>(p => new QuestionnaireFilterClientFactory(p));
            services.AddTransient(p => p.GetService<IQuestionnaireFilterClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }



      
    }
}