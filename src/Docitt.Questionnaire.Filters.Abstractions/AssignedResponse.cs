namespace Docitt.Questionnaire.Filters
    {
    public class AssignedResponse : IAssignedResponse
    {

        public AssignedResponse()
        {
            
        }

        public AssignedResponse(bool isAssigned)
        {
            IsAssigned = isAssigned;
        }


        public bool IsAssigned { get; set; }
    }
}