﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Questionnaire.Filters
{
    public class Section : ISection
    {
        public int SeqNo { get; set; }
        public string SectionName { get; set; }
        public bool IsCompleted { get; set; }
    }
}
