﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using Docitt.AssignmentEngine;

namespace Docitt.Questionnaire.Filters
{
    public class FilterViewResponse : IFilterViewResponse
    {
        public FilterViewResponse()
        {
        }
        public FilterViewResponse(IInvite lead)
        {
            if (lead != null)
            {
                Id = lead.Id;
                TenantId = lead.TenantId;
                Status = FormStatus.Open;
                Applicants = new List<IApplicant>();
                Applicants.Add(new Applicant
                {
                    FirstName = lead.InviteFirstName,
                    LastName = lead.InviteLastName,
                    ApplicantType = lead.Role,
                    Email = lead.InviteEmail,
                    Phone = lead.InviteMobileNumber,
                });

                LastReminder = lead.LastReminder;
                CreatedDate = new TimeBucket(lead.InvitationDate);
                InvitationDate = lead.InvitationDate;
                isLead = true;
                DateTimeSort = lead.InvitationDate;
                InvitationToken = lead.InvitationToken;

            }
        }

        public FilterViewResponse(IFilterView filterview)
        {
            if (filterview != null)
            {
                //var filterviewResponse = new FilterViewResponse();
                Id = filterview.Id;
                TenantId = filterview.TenantId;
                TemporaryApplicationNumber = filterview.TemporaryApplicationNumber;
                Applicants = filterview.Applicants;
                LoanAmount = filterview.LoanAmount;
                LoanType = filterview.LoanType;
                LastLoggedIn = filterview.LastLoggedIn;
                LastReminder = filterview.LastReminder;
                Sections = filterview.Sections;
                CreatedDate = filterview.CreatedDate;
                Status = filterview.Status;
                Assignees = filterview.Assignees;
                if (filterview.CreatedDate != null)
                    DateTimeSort = filterview.CreatedDate.Time;               
            }
        }


        public List<IApplicant> Applicants
        {
            get; set;
        }

        public Dictionary<string, string> Assignees
        {
            get; set;
        }

        public TimeBucket CreatedDate
        {
            get; set;
        }

        public string Id
        {
            get; set;
        }

        public bool isLead
        {
            get; set;
        }

        public DateTimeOffset? LastLoggedIn
        {
            get; set;
        }

        public DateTimeOffset? LastReminder
        {
            get; set;
        }

        public string LoanAmount
        {
            get; set;
        }

        public string LoanType
        {
            get; set;
        }

        public List<ISection> Sections
        {
            get; set;
        }

        public FormStatus Status
        {
            get; set;
        }

        public string TemporaryApplicationNumber
        {
            get; set;
        }

        public string TenantId
        {
            get; set;
        }

        public DateTimeOffset InvitationDate { get; set; }

        public DateTimeOffset DateTimeSort { get; set; }

        public string InvitationToken { get; set; }
    }
}
