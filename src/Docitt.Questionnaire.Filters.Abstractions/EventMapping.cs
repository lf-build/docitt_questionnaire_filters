﻿namespace Docitt.Questionnaire.Filters
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string ApplicationNumber { get; set; }
        public string EntityId { get; set; }
    }
}