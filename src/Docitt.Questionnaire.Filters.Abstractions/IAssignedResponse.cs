namespace Docitt.Questionnaire.Filters
{
     public interface IAssignedResponse
    {
        bool IsAssigned { get; set; }
    }
}