namespace Docitt.Questionnaire.Events
{
    public class ApplicationReminderSend
    {
        public ApplicationReminderSend(ApplicationReminderSend obj)
        {
            this.FirstName = obj.FirstName;
            this.Lender = obj.Lender;
            this.Email = obj.Email;
            this.Name = obj.Name;
        }

        public ApplicationReminderSend(string firstName, string lender, string email, string name)
        {
            this.FirstName = firstName;
            this.Lender = lender;
            this.Email = email;
            this.Name = name;
        }

        public string FirstName { get; set; }
        public string Lender { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}