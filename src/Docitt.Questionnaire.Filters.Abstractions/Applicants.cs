﻿namespace Docitt.Questionnaire.Filters
{
    public class Applicant : IApplicant
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ApplicantType { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string UserName { get; set; }
    }
}
