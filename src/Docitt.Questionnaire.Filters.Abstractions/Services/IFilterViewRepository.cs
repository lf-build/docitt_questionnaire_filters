﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire.Filters
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        Task<IFilterView> GetByUserName(string entityId, string username);
        void AddOrUpdate(IFilterView view);

        Task<IEnumerable<IFilterView>> GetAllApplications();

        Task<IEnumerable<IFilterView>> GetApplications(FormStatus status);
   
        Task<IFilterView> GetApplicationByEntityId(string entityId);

        Task<IEnumerable<IFilterView>> GetUserWiseApplications(string userName);

        Task<IEnumerable<IFilterView>> GetUserAndStatusWiseApplications(string userName, FormStatus status);

        Task<IFilterView> GetApplicationByApplicationNumber(string applicationNumber);

        Task<bool> MapInviteIdToUsername(string inviteId, string userName ,string temporaryApplicationNumber);

        void UpdateLastLoggedInByUserName(string userName, DateTimeOffset? lastLoggedIn);
        
        void UpdateLastReminderSent(string userName, DateTimeOffset lastReminderSent);

        void UpdateStatus(string temporaryApplicationNumber, FormStatus questionnaireStatus);

        Task<string> GetUserNameByApplicationNumberAndRole(string applicationNumber,string role);
    }
}