﻿using LendFoundry.Security.Tokens;

namespace Docitt.Questionnaire.Filters
{
    public interface IFilterViewRepositoryFactory
    {
        IFilterViewRepository Create(ITokenReader reader);
    }
}