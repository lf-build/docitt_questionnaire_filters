﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Questionnaire.Filters
{
    public interface IQuestionnaireFilterService
    {
        Task<IAssignedResponse> IsAssigned(string entityId);
        Task<IEnumerable<IFilterView>> GetAllApplications();

        Task<IEnumerable<IFilterView>> GetApplications(string status);

        Task<IEnumerable<IFilterView>> GetUserWiseApplications();

        Task<IEnumerable<IFilterView>> GetUserAndStatusWiseApplications(string status);

        Task<IFilterView> GetNonSubmittedApplication(string applicationId);

        Task<IEnumerable<IFilterView>> GetAllApplicationsWithLeads();

        Task<IEnumerable<IFilterView>> GetAllApplicationsLeadsWithStatus(string status);

        Task<IEnumerable<IFilterView>> GetUserWiseApplicationsWithLeads();

        Task<bool> IsValidApplicationForUser(string temporaryApplicationNumber);

        Task<List<IApplicationWrapperResponse>> GetUserNameWiseAllApplications();

        Task<List<string>> GetAllApplicationNumbersByUser();

        Task<IFilterView> GetApplicationByApplicationNumber(string applicationNumber);

        Task<ISendReminderEmailResponse> SendReminderEmail(string applicationNumber, string templateName ,string templateVersion, IReminderEmailRequest payload, bool sendemail=true);

        Task<bool> UpdateUsernameField(string inviteId, string userName, string temporaryApplicationNumber);

        Task<bool> RemoveApplicantFromApplication(string applicationNumber,IEnumerable<string> applicantList);

        Task<IEnumerable<string>> GetApplicationsByUser(string userName);

        Task<string> GetUserNameByApplicationNumberAndRole(string applicationNumber,string role);
    }
}