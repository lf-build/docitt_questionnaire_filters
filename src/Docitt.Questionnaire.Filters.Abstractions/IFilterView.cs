﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;

namespace Docitt.Questionnaire.Filters
{
    public interface IFilterView : IAggregate
    {
        string TemporaryApplicationNumber { get; set; }
        List<IApplicant> Applicants { get; set; }
        string LoanType { get; set; }
        string LoanAmount { get; set; }
        DateTimeOffset? LastLoggedIn { get; set; }
        DateTimeOffset? LastReminder { get; set; }
        List<ISection> Sections { get; set; }
        TimeBucket CreatedDate { get; set; }
        FormStatus Status { get; set; }
        Dictionary<string, string> Assignees { get; set; }
    }
}