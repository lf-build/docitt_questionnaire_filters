﻿using System.Collections.Generic;

namespace Docitt.Questionnaire.Filters
{
    public interface IApplicant
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string ApplicantType { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        string UserName { get; set; }
    }

    public enum ApplicantType
    {
        Borrower,
        Spouse,
        CoBorrower
    }
}
