﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Questionnaire.Filters
{
  public  interface IFilterViewResponse : IFilterView
    {
        bool isLead { get; set; }
        DateTimeOffset InvitationDate { get; set; }

        DateTimeOffset DateTimeSort { get; set; }

        string InvitationToken { get; set; }

    }
}
