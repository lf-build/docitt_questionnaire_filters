using Newtonsoft.Json;
using System.ComponentModel;

namespace Docitt.Questionnaire.Filters
{
    public class ReminderEmailRequest : IReminderEmailRequest
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lender")]        
        public string Lender { get; set; }
        [JsonProperty("email")]        
        public string Email { get; set; }
        [JsonProperty("name")]        
        public string Name { get; set; }
    }
}