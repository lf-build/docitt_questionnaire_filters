namespace Docitt.Questionnaire.Filters
{
    public class SendReminderEmailResponse : ISendReminderEmailResponse
    {
        public string ReferenceNumber {get; set;}
        public bool Success { get; set; }
    }
}