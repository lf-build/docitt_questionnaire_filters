using Newtonsoft.Json;
using System.ComponentModel;

namespace Docitt.Questionnaire.Filters
{
    public interface IReminderEmailRequest
    {
        string FirstName { get; set; }
        string Lender { get; set; }
        string Email { get; set; }
        string Name { get; set; }
    }
}