﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;

namespace Docitt.Questionnaire.Filters
{
    public class FilterView : Aggregate, IFilterView
    {
        public string TemporaryApplicationNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IApplicant, Applicant>))]
        public List<IApplicant> Applicants { get; set; }
        public string LoanType { get; set; }
        public string LoanAmount { get; set; }
        public DateTimeOffset? LastLoggedIn { get; set; }
        public DateTimeOffset? LastReminder { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<ISection, Section>))]
        public List<ISection> Sections { get; set; }
        public TimeBucket CreatedDate { get; set; }
        public FormStatus Status { get; set; }

        public Dictionary<string, string> Assignees { get; set; }
    }
}