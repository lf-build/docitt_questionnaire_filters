﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Questionnaire.Filters
{
    public interface ISection
    {
        int SeqNo { get; set; }
        string SectionName { get; set; }
        bool IsCompleted { get; set; }
    }
}
