using LendFoundry.Foundation.Date;

namespace Docitt.Questionnaire.Filters
{

    public interface IApplicationWrapperResponse
    {
         string TemporaryApplicationNumber { get; set; }

         string LoanType { get; set; }

         string LoanStatus { get; set; }

         TimeBucket CreatedDate { get; set; }

    } 
    public class ApplicationWrapperResponse : IApplicationWrapperResponse
    {
        public string TemporaryApplicationNumber { get; set; }

        public string LoanType { get; set; }

        public string LoanStatus { get; set; }

        public TimeBucket CreatedDate { get; set; }

    }
}