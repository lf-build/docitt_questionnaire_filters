namespace Docitt.Questionnaire.Filters
{
    public interface ISendReminderEmailResponse
    {
        string ReferenceNumber {get; set;}
        bool Success { get; set; }
    }
}