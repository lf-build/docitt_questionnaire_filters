﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Questionnaire.Filters
{
    public enum Events
    {
        SectionCompleted,
        UserLoggedIn,
        EmailSent,
        ApplicationSubmitted
    }
}
