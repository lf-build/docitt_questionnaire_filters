﻿using System;

namespace Docitt.Questionnaire.Filters
{
    public class Settings
    {        
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "questionnaire-filters";
      
    }
}