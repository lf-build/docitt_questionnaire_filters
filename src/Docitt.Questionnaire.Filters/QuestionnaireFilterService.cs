﻿using Docitt.AssignmentEngine.Services;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Docitt.Questionnaire.Events;
using LendFoundry.Email;
using LendFoundry.EventHub;

namespace Docitt.Questionnaire.Filters
{
    public class QuestionnaireFilterService : IQuestionnaireFilterService
    {
        public QuestionnaireFilterService
        (
            IFilterViewRepository repository,
            ILogger logger,
            ITenantTime tenantTime,
            Configuration configuration,
            ITokenReader tokenReader,
            ITokenHandler tokenParser,
            IIdentityService identityService,
            IInviteService inviteService,
            IQuestionnaireService questionnaireService,
            IEventHubClient eventHubClient,
            IEmailService emailService
        )
        {
            if (repository == null) throw new ArgumentException($"{nameof(repository)} is mandatory");
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (configuration == null) throw new ArgumentException($"{nameof(configuration)} is mandatory");
            if (tokenReader == null) throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenParser == null) throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            if (identityService == null) throw new ArgumentException($"{nameof(identityService)} is mandatory");
            if (inviteService == null) throw new ArgumentException($"{nameof(inviteService)} is mandatory");
            if (questionnaireService == null) throw new ArgumentException($"{nameof(questionnaireService)} is mandatory");
            if (emailService == null) throw new ArgumentException($"{nameof(emailService)} is mandatory");

            Repository = repository;
            Logger = logger;
            TenantTime = tenantTime;
            Configuration = configuration;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            IdentityService = identityService;
            InviteService = inviteService;
            QuestionnaireService = questionnaireService;
            EventHubClient = eventHubClient;
            EmailService = emailService;
        }

        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }
        private Configuration Configuration { get; }
        private IFilterViewRepository Repository { get; }
        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }
        private IIdentityService IdentityService { get; }

        private IQuestionnaireService QuestionnaireService { get; }
        private IInviteService InviteService { get; }
        private IEmailService EmailService { get; }
        // Event Hub 
        private IEventHubClient EventHubClient { get; }

        public void EnsureEntityId(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is required.");
        }

        public string EnsureCurrentUser()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("username is mandatory");
            return username;
        }

        public async Task<IAssignedResponse> IsAssigned(string entityId)
        {
            EnsureEntityId(entityId);            
            var username = EnsureCurrentUser();

            var assignment = await Repository.GetByUserName(entityId, username);
            return assignment == null ? new AssignedResponse(false) : new AssignedResponse(true);
        }


        public async Task<IEnumerable<IFilterView>> GetAllApplicationsWithLeads()
        {
            var objResponse = new List<FilterViewResponse>();

            // var inviteinfo = await InviteService.GetLeadInviteInformationByInvitee("kinjal.s+110420180617@sigmainfo.net");
            var leadList = await InviteService.GetAllLeadInvite();

            if (leadList != null)
            {
                var mappedleadData = leadList.Select(p => new FilterViewResponse(p)).ToList();
                if (mappedleadData != null)
                    objResponse.AddRange(mappedleadData);
            }

            var appList = await Repository.GetAllApplications();
            if (appList != null)
            {
                var mappedApplicationData = appList.Select(p => new FilterViewResponse(p)).ToList();
                if (mappedApplicationData != null)
                    objResponse.AddRange(mappedApplicationData);
            }

            return objResponse.OrderByDescending(x => x.DateTimeSort);
        }

        public async Task<IEnumerable<IFilterView>> GetAllApplicationsLeadsWithStatus(string status)
        {
            var objResponse = new List<FilterViewResponse>();

            // var inviteinfo = await InviteService.GetLeadInviteInformationByInvitee("kinjal.s+110420180617@sigmainfo.net");
            var leadList = await InviteService.GetAllLeadInvite();

            if (leadList != null)
            {
                var mappedleadData = leadList.Select(p => new FilterViewResponse(p)).ToList();
                if (mappedleadData != null)
                    objResponse.AddRange(mappedleadData);
            }
            FormStatus formStatus = GetFormStatus(status);

            var appList = await Repository.GetApplications(formStatus);

            if (appList != null)
            {

                var mappedApplicationData = appList.Select(p => new FilterViewResponse(p)).ToList();
                if (mappedApplicationData != null)
                    objResponse.AddRange(mappedApplicationData);
            }

            return objResponse.OrderByDescending(x => x.DateTimeSort);
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseApplicationsWithLeads()
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            var objResponse = new List<FilterViewResponse>();

            var leadList = await InviteService.GetAllLeadInvitedBy(userName);

            if (leadList != null)
            {
                var mappedleadData = leadList.Select(p => new FilterViewResponse(p)).ToList();
                if (mappedleadData != null)
                    objResponse.AddRange(mappedleadData);
            }

            var appList = await Repository.GetUserWiseApplications(userName);
            if (appList != null)
            {
                var mappedApplicationData = appList.Select(p => new FilterViewResponse(p)).ToList();
                if (mappedApplicationData != null)
                    objResponse.AddRange(mappedApplicationData);
            }

            return objResponse.OrderByDescending(x => x.DateTimeSort);
        }


        public async Task<IFilterView> GetApplicationByApplicationNumber(string applicationNumber)
        {
            return await Repository.GetApplicationByApplicationNumber(applicationNumber);
        }


        public async Task<IEnumerable<IFilterView>> GetAllApplications()
        {
            return await Repository.GetAllApplications();
        }

        public async Task<IEnumerable<IFilterView>> GetApplications(string status)
        {
            FormStatus formStatus = GetFormStatus(status);

            return await Repository.GetApplications(formStatus);
        }


        public async Task<IEnumerable<IFilterView>> GetUserWiseApplications()
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            return await Repository.GetUserWiseApplications(userName);
        }

        public async Task<List<IApplicationWrapperResponse>> GetUserNameWiseAllApplications()
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            //// Questionnaire filters Application
            var applicationList = (await Repository.GetUserWiseApplications(userName));
            
            //// Application Services from questionnaire
            var submittedApplications = await QuestionnaireService.GetAllSubmittedApplications();

            var result = ProcessedApplications(applicationList,submittedApplications);

            return result;
        }

         public async Task<List<string>> GetAllApplicationNumbersByUser()
        {
            var applicationNumbers = new List<string>();
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            //// Questionnaire filters Application
            var applicationList = (await Repository.GetUserWiseApplications(userName));
            
            //// Application Services from questionnaire
            var submittedApplications = await QuestionnaireService.GetAllSubmittedApplications();

            if(applicationList != null && applicationList.Any())
            {
                var questionnaireApplicationNumbersList = applicationList.Select(x=>x.TemporaryApplicationNumber);
                applicationNumbers.AddRange(questionnaireApplicationNumbersList);
            }

            if(submittedApplications != null && submittedApplications.Any())
            {
                var applicationNumbersList = submittedApplications.Select(x=>x.ApplicationNumber);
                applicationNumbers.AddRange(applicationNumbersList);
            }

            return applicationNumbers.Distinct().OrderByDescending(x=>x).ToList();
        }

        public async Task<bool> IsValidApplicationForUser(string temporaryApplicationNumber)
        {
            if (string.IsNullOrWhiteSpace(temporaryApplicationNumber)){
                return false;
            }
            
            var result = await GetUserNameWiseAllApplications();

            if(result.Any(x=>x.TemporaryApplicationNumber == temporaryApplicationNumber)){
                return true;
            }

            return false;
        }
        
        private List<IApplicationWrapperResponse> ProcessedApplications(IEnumerable<IFilterView> openApplications, List<Docitt.Application.IApplication> submittedApplications)
        {
            var openApplicationsList = new List<ApplicationWrapperResponse>();
            var submitApplicationsList = new List<ApplicationWrapperResponse>();
            if(openApplications != null && openApplications.Any())
            {
                openApplicationsList = openApplications
                                        .Select(x=> new ApplicationWrapperResponse(){
                                                TemporaryApplicationNumber = x.TemporaryApplicationNumber,
                                                LoanStatus = Enum.GetName(typeof(FormStatus), x.Status),
                                                LoanType = x.LoanType,
                                                CreatedDate = x.CreatedDate
                                        }).ToList();
            }
            if(submittedApplications != null && submittedApplications.Any()){
                submitApplicationsList = submittedApplications
                                          .Select(x=> new ApplicationWrapperResponse(){
                                                TemporaryApplicationNumber = x.ApplicationNumber,
                                                LoanStatus = x.Status != null ? x.Status.Name : null,
                                                LoanType = x.Purpose,
                                                CreatedDate = x.SubmittedDate
                                         }).ToList();
            }
            

            var filterlist = submitApplicationsList.Where(p1 => 
                              openApplicationsList.Any(p2 => p1.TemporaryApplicationNumber == p2.TemporaryApplicationNumber)).ToList();
            openApplicationsList.RemoveAll(p1 => submitApplicationsList.Any(p2 => p1.TemporaryApplicationNumber == p2.TemporaryApplicationNumber));
            openApplicationsList.AddRange(filterlist);

            return openApplicationsList.GroupBy(x=> x.TemporaryApplicationNumber).Select(x=>x.First()).OrderByDescending(x=>x.TemporaryApplicationNumber).ToList<IApplicationWrapperResponse>();
        }
        public async Task<IEnumerable<IFilterView>> GetUserAndStatusWiseApplications(string status)
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            FormStatus formStatus = GetFormStatus(status);

            return await Repository.GetUserAndStatusWiseApplications(userName, formStatus);
        }

        /// <summary>
        /// This is to send email for reminder
        /// </summary>
        /// <param name="applicationNumber">application number</param>
        /// <param name="templateName">reminder template name</param>
        /// <param name="templateVersion">reminder template version</param>
        /// <param name="sendemail">bool flag to indicate to send email</param>
        /// <returns></returns>
        public async Task<ISendReminderEmailResponse> SendReminderEmail(string applicationNumber, string templateName ,string templateVersion, IReminderEmailRequest payload, bool sendemail=true)
        {
            ISendReminderEmailResponse response = new SendReminderEmailResponse();
            
            if(sendemail)
            {
                var result = await EmailService.Send("application", applicationNumber, templateName, payload);
                response.Success = true;
            }
            else
            {
                response.Success = true;
                response.ReferenceNumber = "email-disabled";
            }

            Logger.Info($"ApplicationReminderSend for {applicationNumber} email {payload.Email}");
            await EventHubClient.Publish(new ApplicationReminderSend(payload.FirstName,payload.Lender,payload.Email,payload.Name));

            return response;
        }

        /// <summary>
        /// GetNonSubmittedApplication
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public async Task<IFilterView> GetNonSubmittedApplication(string applicationId)
        {
            if (string.IsNullOrWhiteSpace(applicationId))
                throw new ArgumentException("ApplicationId is null or empty");

            return await Repository.GetApplicationByEntityId(applicationId);
        }

        /// <summary>
        /// GetNonSubmittedApplication
        /// </summary>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        public async Task<bool> UpdateUsernameField(string inviteId, string userName, string temporaryApplicationNumber)
        {
            return await Repository.MapInviteIdToUsername(inviteId, userName, temporaryApplicationNumber);
        }


        public  async Task<bool> RemoveApplicantFromApplication(string applicationNumber,IEnumerable<string> applicantList)
        {
             if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("applicationNumber is null or empty");


              var application = await Repository.GetApplicationByApplicationNumber(applicationNumber);
              if(application == null)
              throw new ArgumentException($"Application not found for applicationNumber={applicationNumber}");

              if(application.Applicants.Count > 0)
              {
                 application.Applicants.RemoveAll(x => applicantList.Contains(x.UserName));
                 foreach(var user in applicantList)
                 {
                     application.Assignees.Remove(user);
                 }
              }

            Repository.Update(application);
            return true;
        }

        public async Task<IEnumerable<string>> GetApplicationsByUser(string userName)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("user cannot be found in the request, please verify");
            var applications = await Repository.GetUserWiseApplications(userName);
            return applications.Select(x => x.TemporaryApplicationNumber).ToList();
        }

        public async Task<string> GetUserNameByApplicationNumberAndRole(string applicationNumber,string role)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("applicationNumber cannot be found in the request, please verify");

             if (string.IsNullOrWhiteSpace(role))
                throw new ArgumentException("role cannot be found in the request, please verify");
         
         return await Repository.GetUserNameByApplicationNumberAndRole(applicationNumber,role);

        }

        private string GetTokenUserName()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            return token.Subject;
        }

        private FormStatus GetFormStatus(string status)
        {
            try
            {
                return (FormStatus)Enum.Parse(typeof(FormStatus), status);
            }
            catch (Exception ex)
            {
                Logger.Error($"Form Status not found : {status}", ex);
                throw new ArgumentException($"Form Status not found : {status}");
            }
        }
    }
}