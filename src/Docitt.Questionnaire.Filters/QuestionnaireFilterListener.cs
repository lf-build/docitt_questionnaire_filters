﻿using Docitt.Questionnaire.Client;
using LendFoundry.ActivityLog;
using LendFoundry.ActivityLog.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using Docitt.AssignmentEngine.Client;
using Docitt.AssignmentEngine;
using LendFoundry.Foundation.Listener;

namespace Docitt.Questionnaire.Filters
{
    public class QuestionnaireFilterListener : ListenerBase, IQuestionnaireFilterListener
    {
        public QuestionnaireFilterListener
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            IFilterViewRepositoryFactory repositoryFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IQuestionnaireServiceClientFactory questionnaireServiceClientFactory,
            IActivityLogClientFactory activityLogClientFactory,
            IIdentityServiceFactory identityServiceFactory,
            IAssignmentServiceClientFactory assignmentFactory
        ) : base( tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)

        {
            EventHubFactory = eventHubFactory ?? throw new ArgumentException($"{nameof(eventHubFactory)} is mandatory");
            ConfigurationFactory = configurationFactory ?? throw new ArgumentException($"{nameof(configurationFactory)} is mandatory");
            TokenHandler = tokenHandler ?? throw new ArgumentException($"{nameof(tokenHandler)} is mandatory");
            RepositoryFactory = repositoryFactory ?? throw new ArgumentException($"{nameof(repositoryFactory)} is mandatory");
            QuestionnaireServiceClientFactory = questionnaireServiceClientFactory ?? throw new ArgumentException($"{nameof(questionnaireServiceClientFactory)} is mandatory");
            ActivityLogClientFactory = activityLogClientFactory ?? throw new ArgumentException($"{nameof(activityLogClientFactory)} is mandatory");
            IdentityServiceFactory = identityServiceFactory ?? throw new ArgumentException($"{nameof(identityServiceFactory)} cannot be null");
            AssignmentFactory = assignmentFactory ?? throw new ArgumentException($"{nameof(assignmentFactory)} is mandatory");
        }

        private IQuestionnaireServiceClientFactory QuestionnaireServiceClientFactory { get; }
        private IActivityLogClientFactory ActivityLogClientFactory { get; }
        private IIdentityServiceFactory IdentityServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IFilterViewRepositoryFactory RepositoryFactory { get; }

        private IAssignmentServiceClientFactory AssignmentFactory { get; }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var repository = RepositoryFactory.Create(reader);
            var questionnaireService = QuestionnaireServiceClientFactory.Create(reader);
            var activityLogService = ActivityLogClientFactory.Create(reader);
            var identityService = IdentityServiceFactory.Create(reader);
            var assignmentService = AssignmentFactory.Create(reader);

            var eventhub = EventHubFactory.Create(reader);
          
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            if (configuration == null)
            {
                logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                return null;
            }

            logger.Info($"#{configuration.Events.Length} events found from configuration: {Settings.ServiceName}");
            var uniqueEvents = configuration.Events
                  
                .Distinct()
                .ToList();
               
            uniqueEvents.ForEach(eventConfig =>
            {
                if (eventConfig.Name == Events.EmailSent.ToString() || eventConfig.Name == Events.ApplicationSubmitted.ToString())
                {
                    eventhub.On(eventConfig.Name, UpdateView(eventConfig, repository, logger, activityLogService, questionnaireService, eventConfig.Name));
                }
                else if (eventConfig.Name == Events.UserLoggedIn.ToString())
                {
                    eventhub.On(eventConfig.Name, UpdateViewLastLoggedId(eventConfig, repository, logger, identityService));
                }
                else
                {
                    eventhub.On(eventConfig.Name, AddView(eventConfig, repository, logger, questionnaireService, identityService, assignmentService, activityLogService));
                }
                
                logger.Info($"It was made subscription to EventHub with the Event: #{eventConfig.Name} for tenant {tenant}");
            });
            return uniqueEvents.Select(p => p.Name).Distinct().ToList();
        }


        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            return configuration?.Events.Select(evnt => evnt.Name).Distinct().ToList();
        }


   
        private static Action<EventInfo> AddView
       (
           EventMapping eventConfiguration,
           IFilterViewRepository repository,
           ILogger logger,
           IQuestionnaireService questionnaireService,
           IIdentityService identityService,
           IAssignmentService assignmentService,
           IActivityLogService activityLogService
       )
        {
            return @event =>
            {
                try
                {
                    var applicationNumber = eventConfiguration.ApplicationNumber.FormatWith(@event);
                    logger.Info($"Processing event {eventConfiguration.Name} for application {applicationNumber}");

                    var application = questionnaireService.GetApplication(applicationNumber).Result;
                    if (application != null)
                    {
                        var filterView = new FilterView()
                        {
                            TemporaryApplicationNumber = application.TemporaryApplicationNumber,
                            LoanType = application.LoanPurpose,
                            LoanAmount = application.LoanAmount,
                            CreatedDate = application.CreatedDate,
                            Status = application.Status
                        };

                        var applicants = new List<IApplicant>();

                        if (application.BorrowerCoBorrowerList != null && application.BorrowerCoBorrowerList.Any())
                        {
                            foreach (var applicant in application.BorrowerCoBorrowerList)
                            {
                                applicants.Add(SetAppicantFilterView(applicant));

                                if (applicant.ApplicantType.ToString() == ApplicantType.Borrower.ToString())
                                {
                                    var user = identityService.GetUser(applicant.UserName.ToLower()).Result;

                                    if (user != null)
                                        filterView.LastLoggedIn = user.LastLoggedIn;
                                }
                            }
                        }
                        
                        var allUserLogs = activityLogService.GetActivitiesByTag("application",
                            application.TemporaryApplicationNumber, new[] {"Reminder"});

                        var lastReminderLog = allUserLogs?.Where(x => x.Tags.Contains("Reminder"))
                            .OrderByDescending(x => x.ActivityDate)
                            .FirstOrDefault();

                        if (lastReminderLog != null)
                            filterView.LastReminder = lastReminderLog.ActivityDate;
                        
                        filterView.Applicants = applicants;

                        var sections = new List<ISection>();

                        if (application.Sections != null && application.Sections.Any())
                        {
                            foreach (var section in application.Sections)
                            {
                                sections.Add(SetSection(section));
                            }
                        }

                        filterView.Sections = sections;

                        // gets all application assignees
                        var assignees = new Dictionary<string, string>();
                        var assignments = assignmentService.Get("Application", applicationNumber).Result?.ToList();

                        if (assignments != null && assignments.Any())
                        {
                            foreach(var asgn in assignments)
                            {
                                var key=asgn.Assignee.ToLower();
                                if(!assignees.ContainsKey(key))
                                    assignees.Add(key,asgn.Role);
                                else
                                {
                                    logger.Warn($"Duplicate assignee {asgn.Assignee} in {applicationNumber}");
                                }
                            }
                            
                            //assignees = assignments.ToDictionary(x => x.Assignee, x => x.Role);
                        }
                        else
                        {
                            logger.Debug($"No assignments found for this snapshot using EntityId#{applicationNumber}");
                        }
                            
                        filterView.Assignees = assignees;
                        repository.AddOrUpdate(filterView);
                        logger.Info($"Added new snapshot added to application {applicationNumber} with status {filterView.Status}");
                    }
                    else
                        logger.Warn($"Application #{applicationNumber} could not be found while processing subscription #{@event.Name}");
                }
                catch (Exception ex) { logger.Error($"Unhadled exception while listening event {@event.Name}", ex); }
            };
        }

        private static Action<EventInfo> UpdateView
        (
            EventMapping eventConfiguration,
            IFilterViewRepository repository,
            ILogger logger,
            IActivityLogService activityLogService,
            IQuestionnaireService questionnaireService,
            string eventName
        )
        {
            return  @event =>
            {
                try
                {
                    var temporaryApplicationNumber = eventConfiguration.EntityId.FormatWith(@event);
                    logger.Info($"Processing event {eventConfiguration.Name} for application {temporaryApplicationNumber}");

                    var application = repository.GetApplicationByEntityId(temporaryApplicationNumber).Result;
                    if (application != null)
                    {
                        if (eventName == Events.EmailSent.ToString())
                        {
                            var allUserLogs = activityLogService.GetActivitiesByTag("application",
                                temporaryApplicationNumber, new[] {"Reminder"});

                            var lastReminderLog = allUserLogs?.Where(x => x.Tags.Contains("Reminder"))
                                .OrderByDescending(x => x.ActivityDate)
                                .FirstOrDefault();

                            if (lastReminderLog != null)
                            {
                                repository.UpdateLastReminderSent(temporaryApplicationNumber,
                                    lastReminderLog.ActivityDate);
                            }

                            logger.Info($"Last Reminder updated in application {temporaryApplicationNumber}");
                        }
                        else if (eventName == Events.ApplicationSubmitted.ToString())
                        {
                            repository.UpdateStatus(temporaryApplicationNumber, FormStatus.PostApplication);

                            logger.Info($"Status updated in application {temporaryApplicationNumber} to status #{FormStatus.PostApplication.ToString()}");
                        }
                    }
                    else
                        logger.Warn(
                            $"Application #{temporaryApplicationNumber} could not be found while processing subscription #{@event.Name}");
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhadled exception while listening event {@event.Name} for {@event.TenantId} ", ex, @event);
                }
            };
        }

        private static Action<EventInfo> UpdateViewLastLoggedId
       (
           EventMapping eventConfiguration,
           IFilterViewRepository repository,
           ILogger logger,
           IIdentityService identityService
       )
        {
            return @event =>
            {
                try
                {
                    var userName = eventConfiguration.Username.FormatWith(@event);

                    // username should be case insensitive and in lower case
                    if (!string.IsNullOrEmpty(userName)) userName = userName.ToLower();

                    var user = identityService.GetUser(userName).Result;

                    if (user != null)
                        repository.UpdateLastLoggedInByUserName(userName, user.LastLoggedIn);

                    logger.Info($"New snapshot added to user {userName}");

                }
                catch (Exception ex)
                {
                    logger.Error($"Unhadled exception while listening event {@event.Name} for {@event.TenantId} ", ex, @event);
                }
            };
        }

        // Creates Applicant Object
        private static IApplicant SetAppicantFilterView(IBorrowerCoBorrowerInfo applicant)
        {
            return new Applicant()
            {
                FirstName = applicant.FirstName,
                LastName = applicant.LastName,
                ApplicantType = applicant.ApplicantType.ToString(),
                Email = applicant.Email,
                Phone = applicant.Phone,
                UserName = applicant.UserName.ToLower()
            };
        }

        // Creates Section Object
        private static ISection SetSection(ISectionBasic section)
        {
            return new Section()
            {
                SeqNo = section.SeqNo,
                SectionName = section.SectionName,
                IsCompleted = section.IsCompleted
            };
        }
    }
}