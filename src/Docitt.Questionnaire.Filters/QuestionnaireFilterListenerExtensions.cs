﻿#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
#endif
namespace Docitt.Questionnaire.Filters
{
    public static class QuestionnaireFilterListenerExtensions
    {
        public static void UseQuestionnaireFilterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IQuestionnaireFilterListener>().Start();
        }
    }
}