# Service Name : lf_questionnairefilters

- The purpose of questionnairefilters service is to send questionnairefilters using settings provided via configuration service. 
- It utilizes template-manager to generate html for questionnairefilters.

## Getting Started

- Require environment setup.
- Repository and MongoDB access.
- Should have knowledge of Bit-bucket and DotNet CLI(Command-line interface ) commands. 
- Helpful links to start work :
	* https://docs.microsoft.com/en-us/dotnet/core/
	* https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x
		
### Prerequisites

 - Windows, macOS or Linux Operating System.
 - .NET Core SDK (Software Development Kit).
 - .NET Core Command-line interface (CLI) or Visual Studio 2017.
 - MongoDB Management Tool.
 
### Installing

 - Clone source using below git command or source tree

	    git clone <repository_path>
	And go to respective branch for ex develop

		git fetch && git checkout develop	

 - Open command prompt and goto project directory.

 - Restore nuget packages by command

 		dotnet restore -s <Nuget Package Source> <solution_file>

		Ex. dotnet restore -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc LendFoundry.Questionnaire.Filters.2017.sln 

 - Build solution by command : 

		dotnet build --no-restore <solution_file>

		Ex. dotnet build --no-restore LendFoundry.Questionnaire.Filters.2017.sln
		
 - Run Solution by command
 
		dotnet run --no-build --project <project path>|
		eg. dotnet run --no-build --project src\LendFoundry.Questionnaire.Filters.Api\LendFoundry.Questionnaire.Filters.Api.csproj
		
## Running the tests

- We have created shellscript file(cover.sh) for running tests and code coverage, which are shared on lendfoundry documents.
- To start the test
	* open cmd 
	* goto project directory
	* type command cover.sh and enter
- you can see all tests results and code coverate report with covered number of lines and percentage on cmd.
- Once testing completed successfully from above command code coverage report html file is geneated on path :
 artifacts\coverage\report\coverage_report\index.html 

### What these tests test and why

- We are using xUnit testing and implementing for below projects.
	* Api
	* Client
	* Service
- xUnit.net is a free, open source, community-focused unit testing tool for the .NET Framework.
- This test includes all the scenarios which can be happen on realistic usage of this service.
- Help URL : https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test

## Service Configuration

- Name :  questionnairefilters .
- Can get configuration by request below URL and token for specific tenant from postman
	* URL : <configuration_service>/questionnairefilters
	* Sample configurations :
	```
	{
		"questionnairefiltersProvider": <Sendgrid or Smtp>,
		"questionnairefiltersProviderSettings": {
			//If questionnairefilters provider is Sendgrid
			"Sendgrid": {
				"ApiKey": <key provided by sendgrid>,
				"FromName": "LendFoundry Development",
				"FromAddress": "development@lendfoundry.com"
			},
			//If questionnairefilters provider is Smtp
			"Smtp": {				
				"FromName": "LendFoundry Development",
				"FromAddress": "development@lendfoundry.com"
				"Host": <server url>,
				"Port": <port number>,
				"UserName": <account username>,
				"Password": <account password>
			}
		},
		"dependencies": {
			"lookup": <lookup service URL>,
			"document_manager": <document manager service URL>,
			"template_manager": <template manager service URL>,
		},
		"ConnectionString": <datdabase connection string>,
		"Database": "questionnairefilters"
	}
	```


## Service Rules

NA

## Service API Documentation

- Anyone can access Api end points by entering below URL in any browser whenever service is running locally

		http://localhost:5000/swagger/

## Databases owned by questionnairefilters service

- Database : MongoDB
- Collections : questionnairefilters

## External Services called

- Sendgrid : Version= [9.9.0] 
	* This library is used for send questionnairefilters functionality.	
- MimeKit : Version= [2.0.2] 
	* MimeKit is a C# library which may be used for the creation and parsing of messages using the Multipurpose Internet Mail Extension (MIME).
- MailKit : Version= [2.0.2] 
	* MailKit is a cross-platform mail client library built on top of MimeKit.

## Environment Variables used by questionnairefilters service

- Envirounment variables are application variables which are used for setting application behaviour at runtime. Ex. We can use different envirounment variables for different servers like DEV, QA etc.
- In lendfoundry any single service is dependent or uses many other lendfoundry services, So for dynamically target different services we are setting envirounment variable in launchSettings.json.
- Below are the envirounment variables in which we are setting only
important services URL's which are used in mostly all lendfoundry services and we are setting it in "src\LendFoundry.Questionnaire.Filters.Api\Properties\launchSettings.json".
- Other dependent service URL's are taken from configurations.

		"CONFIGURATION_NAME": "questionnairefilters",
		"CONFIGURATION_URL": <configuration service URL>,
		 // ex. "CONFIGURATION_URL": "http://foundation.dev.lendfoundry.com:7001",
		"EVENTHUB_URL": <event hub URL>,
		"NATS_URL": <nats server URL>,
		"TENANT_URL": <tenant server URL>,
		"LOG_LEVEL": "Debug"

## Known Issues and Workarounds

## Changelog